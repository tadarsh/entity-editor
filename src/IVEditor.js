import {React, useCallback} from "react";
import { Slate, Editable } from "slate-react";
import {Editor, Transforms, Element as SlateElement } from "slate";

function IVEditor({className, editor, text, setText, allowNewLine, placeholder}) {
    
    const renderLeaf = useCallback(props => <Leaf {...props}/>, [])
    const renderElement = useCallback(props => <Element {...props} />, [])

    return (
        <div className={className}>
            <Slate
                editor={editor}
                value={text}
                onChange={setText}
            >
                <Editable 
                    placeholder={placeholder || "Untitled Note"}
                    renderLeaf={renderLeaf}
                    renderElement={renderElement}

                    onKeyDown={(event) => { 
                            if (event.key === "Enter") {
                                if (!allowNewLine)
                                    event.preventDefault()
                            }
                        }
                    }
                />
            </Slate>
        </div>
    )
}

function toggleMark(editor, format) {
    const isActive = isMarkActive(editor, format)

    if (isActive) {
      Editor.removeMark(editor, format)
    } else {
      Editor.addMark(editor, format, true)
    }
}


function isMarkActive(editor, format) {
    const marks = Editor.marks(editor)
    return marks ? marks[format] === true : false
}

function Leaf ({ attributes, children, leaf}) {
    if (leaf.bold) {
      children = <strong>{children}</strong>
    }
  
    if (leaf.code) {
      children = <code>{children}</code>
    }
  
    if (leaf.italic) {
      children = <em>{children}</em>
    }
  
    if (leaf.underline) {
      children = <u>{children}</u>
    }

    if (leaf.uid) {
        return <span {...attributes} className={leaf.uid}>{children}</span>
    } else {
        return <span {...attributes}>{children}</span>
    }

}

const LIST_TYPES = ['numbered-list', 'bulleted-list']

function toggleBlock(editor, format) {
    const isActive = isBlockActive(editor, format)
    const isList = LIST_TYPES.includes(format)
  
    Transforms.unwrapNodes(editor, {
      match: n =>
        !Editor.isEditor(n) &&
        SlateElement.isElement(n) &&
        LIST_TYPES.includes(n.type),
      split: true,
    })
    const newProperties = {
      type: isActive ? 'paragraph' : isList ? 'list-item' : format,
    }
    Transforms.setNodes(editor, newProperties)
  
    if (!isActive && isList) {
      const block = { type: format, children: [] }
      Transforms.wrapNodes(editor, block)
    }
}

function isBlockActive(editor, format) {
    const { selection } = editor
    if (!selection) return false
  
    const [match] = Array.from(
      Editor.nodes(editor, {
        at: Editor.unhangRange(editor, selection),
        match: n =>
          !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === format,
      })
    )
  
    return !!match
}

function Element({ attributes, children, element }) {
    switch (element.type) {
      case 'block-quote':
        return <blockquote {...attributes}>{children}</blockquote>
      case 'bulleted-list':
        return <ul {...attributes}>{children}</ul>
      case 'heading-one':
        return <h1 {...attributes}>{children}</h1>
      case 'heading-two':
        return <h2 {...attributes}>{children}</h2>
      case 'list-item':
        return <li {...attributes}>{children}</li>
      case 'numbered-list':
        return <ol {...attributes}>{children}</ol>
      default:
        return <p {...attributes}>{children}</p>
    }
}

const initialValue = (text) => {
  return [
      {
          type: 'paragraph',
          children: [{ text: text }],
      },
  ]
}


export {IVEditor, toggleMark, toggleBlock, initialValue};