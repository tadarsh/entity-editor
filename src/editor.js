import Split from "react-split"
import "./index.css"
import React, { useRef, useState } from "react";
import { IVEditor, initialValue, toggleMark } from "./IVEditor";
import { createEditor } from "slate";
import { withReact } from "slate-react";

export default function Editor() {
    const [editor] = useState(() => withReact(createEditor()));
    const [text, setText] = useState(initialValue(''));

    const handleClick = (event) => {
        event.preventDefault()
        toggleMark(editor, 'bold')
    }

    return (
        <div>
            <button onMouseDown={handleClick}>Highlight</button>
            <Split 
                className="wrap"
                gutterSize={2}
            >
                <div className="a1">
                    <h1>Editor</h1>
                    <IVEditor 
                        className="hightext"
                        editor={editor}
                        text={text}
                        setText={setText}
                        allowNewLine={true}
                        placeholder={"Untitled note"}
                    />
                </div>
                <div>
                    <h1>Tags go here</h1>
                </div>
            </Split>
        </div>
    )
}